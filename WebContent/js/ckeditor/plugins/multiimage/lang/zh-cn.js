﻿/**
 * 支持英文的语言包(文件名称zh-cn.js)，第一个参数是插件名称
 */
CKEDITOR.plugins.setLang('multiimage', 'zh-cn', {
	tbTip    : '',
	mytxt    : '文本',
	dlgTitle : '',
	editor.lang.mine:'多图片上传'
});