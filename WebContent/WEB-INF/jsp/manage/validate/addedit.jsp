<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>添加/编辑</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    

    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<style type="text/css">
	.forms{
		width:800px;
		padding:10 10 10 10;
	}
	.forms table tr{
		padding-top:10px;
	}
	.forms table tr th{
		padding-top:10px;
		padding-bottom:5px;
		text-align:left;
	}
	.forms table tr td{
		padding-top:10px;
		padding-bottom:5px;
	}
	.forms table tr .title{
		padding-left:10px;
		width:30%;
		border-bottom:solid 1px #df585f;
	}
	.forms table tr .val{
		padding-left:10px;
		width:50%;
		border-bottom:solid 1px #df585f;
	}
	.forms table tr .val input{
		width:100%;
	}
	.forms table tr .val textarea{
		width:100%;
	}
	.forms table tr .reg{
		padding-left:5px; 
		width:20%;
	}
	.forms table tr td .reg .Validform_checktip{
		font-size:12px;
		color:red;
	}
	</style>
  </head>
  
  <body>
    <form id="fm" class="forms" action="<%=basePath%>manage/validate/doAdd.do" method="post">
    	<table cellpadding="0" cellspacing="0" border="0">
    		<tr>
    			<th class="title">验证模型名称</th>
    			<td class="val">
    				<input type="text" class="inputxt" name="validateName" tip="输入验证模型名称" datatype="*3-10" errormsg="名称长度不得小于3且不得大于10"/></div>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></td>
    		</tr>
    		<tr>
    			<th class="title">验证模型类型</th>
    			<td class="val">
    				<input type="text" class="inputxt" name="validateType" tip="输入验证模型类型" datatype="n1-1"  errormsg="0-9的整数"/></div>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></td>
    		</tr>
    		<tr>
    			<th class="title">验证模型表达式</th>
    			<td class="val">
    				<input type="text" class="inputxt" name="dataType" tip="输入验证模型表达式" datatype="*1-10"  errormsg="参考validformde的tatype"/></div>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></td>
    		</tr>
    		<tr>
    			<th class="title">异步验证的url地址</th>
    			<td class="val">
    				<input type="text" class="inputxt" name="ajaxUrl" tip="输入有效的url地址" datatype="url"  errormsg="输入有效的url地址"/></div>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></td>
    		</tr>
    		<tr>
    			<th class="title">验证模型比对标签</th>
    			<td class="val">
    				<input type="text" class="inputxt" name="rechek" tip="输入验证模型比对标签的name属性" datatype="*1-10"  errormsg="输入验证模型比对标签的name属性"/></div>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></td>
    		</tr>
    		<tr>
    			<th class="title">验证错误提示信息</th>
    			<td class="val">
    				<input type="text" class="inputxt" name="errormsg" tip="输入验证错误提示信息" datatype="*1-100"  errormsg="输入验证错误提示信息"/></div>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></td>
    		</tr>
    		<tr>
    			<th class="title">验证模型附加javascript函数、代码</th>
    			<td class="val">
    				<textarea name="func" rows="10" tip="输入验证模型附加javascript函数、代码" datatype="*1-255"  errormsg="输入验证错误提示信验证模型附加javascript函数、代码"></textarea></div>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></td>
    		</tr>
    	</table>
    	<input type="submit"/>
    </form> 
    <script type="text/javascript">
    $(function(){
		
	$("#fm").Validform({
		tiptype:2,
		callback:function(form){
			var check=confirm("您确定要提交表单吗？");
			if(check){
				form[0].submit();
			}
			
			return false;
		}
		
	});
})
</script>

  </body>
</html>
