<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${pageTitle }</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript">
		var basePath='<%=path %>';
	</script>
	<style type="text/css">
	#bd{
		width:320px;
		margin:0 auto;
	}
	#bd table tr{
		padding-top:10px;
		padding-bottom:10px;
		height:40px;
	}
	#bd table tr th label{
		font-size:16px;
		margin-right:15px;
		width:150px;
	}
	#bd table tr td input{
		font-size:18px;
		margin-right:15px;
	}
	</style>
	</head>

	<body style="width:100%;text-align:center;">
	<div id="bd">
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<th><label>输入新密码:</label></th>
				<td><input type="password" id="newpsd"/></td>
			</tr>
			<tr>
				<th><label><label>再次输入:</label></label></th>
				<td><input type="password" id="newpsd2"/></td>
			</tr>
		</table>
	</div>

	
	<script type="text/javascript">
	var rtn=false;
	var servDataRtn=false;
	$(document).ready(function(){
		rtn=false;
		
	});
		function submit(){
			if($("#newpsd").val()!=$("#newpsd2").val()){
			msgShow("警告","两次输入密码不一致","warning");
			
			}else{
				$.ajax({
					 type: "POST",
					 url: basePath+"/Manage/User/doEditCurrentUserPassword.do",
					 data: {"psd":$("#newpsd").val()},
					 dataType: "text",
					 async:false,
					 success: function(result){
						if(result.indexOf("msg")!=0){
							var t=eval("("+result+")");
							if(t.status='99'){
								rtn=true;
							}else{
								alert(t.msg);
							}
						}
					}
				 });
			return rtn;
			}
		};
 	function msgShow(title, msgString, msgType) {
		$.messager.alert(title, msgString, msgType);
	};
				
     </script>
	</body>
</html>
