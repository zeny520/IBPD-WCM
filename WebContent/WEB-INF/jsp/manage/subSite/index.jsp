<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${pageTitle }</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/custom.js"></script>
	<script type="text/javascript">
		var basePath='<%=path %>';
	</script>
	</head>

	<body>
	<div class="easyui-layout" fit="true">
    <div id="nodeview" region="center" style="background: #eee; overflow-y:hidden">
        <div id="tabs" tabPosition="bottom" class="easyui-tabs"  fit="true" border="false" >
			<div title="站点管理" style="padding:0;overflow:hidden; color:red; " >
			     <table id="grid" style="width: 900px;height:auto;" title="站点管理" iconcls="icon-view">            
           		 </table>
			</div>
		</div>
    </div>
   <div region="east" hide="true" split="false" title="属性配置" style="width:230px;" id="west">
	    <iframe id="propsPanel" onreadystatechange="resize()"  src="<%=path %>/Manage/SubSite/props.do?id=-1" scrolling="auto"  frameborder="0" style="width:100%;height:100%;"></iframe>
    </div>
	</div>
	
	<script type="text/javascript">
	$(document).ready(function(){
		//initTabs();
		InitGrid();//默认没有传参数,参数应该是querydata
	});
	 var oTime = null;
    function resize()
    {
        if(oTime)
        {
            clearTimeout(oTime);
        }
         
        oTime = setTimeout(reset, 200);
    }
     
    function reset()
    {
        var frame = document.getElementById("propsPanel");
        var outHeight = frame.offsetHeight;
        var inHeight = frame.contentWindow.document.body.scrollHeight;
        if(outHeight != inHeight)
        {
            frame.style.height = (inHeight + 10) + "px";
        }
    }
	function reload(){
		$('#grid').datagrid("unselectAll");
		$('#grid').datagrid("reload");
	};
	function del(){
		var _ids=getSelections("table_content");
		if(_ids.length==0){
			msgShow("提示","没有选中行","warning");
		}else{
		$.messager.confirm("确认","确定删除吗?",function(r){
			if(r){
				$.post(
					basePath+"/Manage/SubSite/doDel.do",
					{ids:_ids},
					function(result){
						msgShow("提示","删除成功.","warning");
						reload();
					}
				);
			}
		});
		}
	};
      function InitGrid(queryData) {
            $('#grid').datagrid({   //定位到Table标签，Table标签的ID是grid
                url: '<%=basePath %>Manage/SubSite/list.do?t='+new Date(), 
                title: '',
                iconCls: 'icon-grid',
                singleSelect:false,
                fit:true,
                width: function () { return document.body.clientWidth * 0.98 },
                nowrap: true,
                striptd:true,
                loadMsg:'数据加载中,请稍后……',
                autoRowHeight: false,
                striped: true,
                collapsible: false,
                pagination: true,
                pageSize: 10,
                pageList: [10,20,30,50,100],
                rownumbers: true,
                //sortName: 'ID',    //根据某个字段给easyUI排序
                sortOrder: 'asc',
                remoteSort: true,
                fitColumns:true,
                idField: 'id',
                queryParams: queryData,  //异步查询的参数 
                onHeaderContextMenu: function(e, field){
                    e.preventDefault();
                    if (!cmenu){
                        createColumnMenu("grid");
                    }
                    cmenu.menu('show', {
                        left:e.pageX,
                        top:e.pageY
                    });
                },
                onRowContextMenu:function(e, rowIndex, rowData){
        			//e.preventDefault();
        			//$('#grid').datagrid('uncheckAll');
                   // $('#grid').datagrid('checkRow', rowIndex);
				   // $('#nodeCtxMenu').menu('show', {
				   //     left:e.pageX,
				   //     top:e.pageY
				   // });    
   				},
   				rowStyler:function(index,row){
   					//if(row.state<=0){
   					//return "background-color:RGB(79,129,189);";
   					//}else{
   					//}
   				},
                columns: [[
					{ field: 'ck', checkbox: true,title:'选择' },
                    { title: 'ID', field: 'id', width: 10, sortable:true,hidden:true },
					{ title: '站点名称', field: 'siteName', width: 10, sortable:true,formatter:function(val, rowdata, index){if(val==null){return ""};if(rowdata.icon.length>1){return "<span><img src='"+rowdata.icon+"' width='16px' height='16px'/>&nbsp;"+val+"</span>"}else{return val}}},
					{ title: '站点全称', field: 'fullName', width: 10, sortable:true,hidden:true },
					{ title: '站点排序', field: 'order', width: 10, sortable:true },
					{ title: '是否锁定', field: 'isLocked', width: 10, sortable:true,formatter:function(val, rowdata, index){if(val==null){return ""}if(val<=0){return "锁定";}else{return "正常";}}},
					{ title: '站点map图片', field: 'map', width: 10, sortable:true,hidden:true },
					{ title: '起始日期', field: 'startDate', width: 10, sortable:true,formatter:function(val, rowdata, index){if(val==null){return ""}return 1900+val.year+"-"+(1+val.month)+"-"+val.date+" 星期"+(1+val.day)}},
					{ title: '结束日期', field: 'endDate', width: 10, sortable:true,formatter:function(val, rowdata, index){if(val==null){return ""}return 1900+val.year+"-"+(1+val.month)+"-"+val.date+" 星期"+(1+val.day)}},
					{ title: '管理地址', field: 'manageUrl', width: 10, sortable:true,hidden:true },
					{ title: '站群ID', field: 'siteGroupId', width: 10, sortable:true,hidden:true },
					{ title: '管理者', field: 'manageUser', width: 10, sortable:true,hidden:true },
					{ title: '关键字', field: 'keywords', width: 10, sortable:true,hidden:true },
					{ title: '背景音乐', field: 'bgMusic', width: 10, sortable:true,hidden:true },
					{ title: '站点logo', field: 'logo', width: 10, sortable:true,hidden:true },
					{ title: '站点ICON', field: 'icon', width: 10, sortable:true,hidden:true },
					{ title: '站点banner', field: 'banner', width: 10, sortable:true,hidden:true },
					{ title: 'copyRight', field: 'copyright', width: 10, sortable:true,hidden:true },
					{ title: '所在目录', field: 'directory', width: 10, sortable:true,hidden:true },
					{ title: '域名', field: 'domainName', width: 10, sortable:true,hidden:true },
					{ title: '创建人', field: 'createUser', width: 10, sortable:true,hidden:true },
					{ title: '创建日期', field: 'createDate', width: 10, sortable:true,hidden:true,formatter:function(val, rowdata, index){return 1900+val.year+"-"+(1+val.month)+"-"+val.date+" "+val.hours+":"+val.minutes+":"+val.seconds+" 星期"+(1+val.day)}},
					{ title: '审核日期', field: 'passedDate', width: 10, sortable:true,hidden:true,formatter:function(val, rowdata, index){return 1900+val.year+"-"+(1+val.month)+"-"+val.date+" "+val.hours+":"+val.minutes+":"+val.seconds+" 星期"+(1+val.day)}},
					{ title: '站点页面模版', field: 'sitePageTemplateId', width: 10, sortable:true,hidden:true },
					{ title: '站点样式模版', field: 'siteStyleTemplateId', width: 10, sortable:true,hidden:true },
					{ title: '栏目页面模版', field: 'nodePageTemplateId', width: 10, sortable:true,hidden:true },
					{ title: '栏目样式模版', field: 'nodeStyleTemplateId', width: 10, sortable:true,hidden:true },
					{ title: '内容页面模版', field: 'contentpageTemplateId', width: 10, sortable:true,hidden:true },
					{ title: '内容样式模版', field: 'contentStyleTemplateId', width: 10, sortable:true,hidden:true },
					{ title: 'FTP服务器地址', field: 'ftpAddress', width: 10, sortable:true,hidden:true },
					{ title: 'FTP服务端口', field: 'ftpPort', width: 10, sortable:true,hidden:true },
					{ title: 'FTP服务用户名', field: 'ftpUserName', width: 10, sortable:true,hidden:true },
					{ title: 'FTP服务密码', field: 'ftpPassword', width: 10, sortable:true,hidden:true,formatter:function(val, rowdata, index){return "**********"}},
					{ title: '站点简介', field: 'description', width: 10, sortable:true,hidden:true }

              ]], 
                toolbar: [{
                    id: 'btnAdd',
                    text: '添加',
                    iconCls: 'icon-add',
                    handler: function () {
                        ShowAddDialog();
                    }
                },{
                    id: 'btnEdit',
                    text: '修改',
                    iconCls: 'icon-edit',
                    handler: function () {
                        ShowEditDialog();//实现修改记录的方法
                    }
                }, {
                    id: 'btnDelete',
                    text: '删除',
                    iconCls: 'icon-remove',
                    handler: function () {
                        del();//实现直接删除数据的方法
                    }
                },{
                    id: 'btnFtpConfig',
                    text: 'FTP设置',
                    iconCls: 'icon-ftp',
                    handler: function () {
                        ftpConfig();//实现直接删除数据的方法
                    }
                },'-', {
                    id: 'btnReload',
                    text: '刷新',
                    iconCls: 'icon-reload',
                    handler: function () {
                        reload();
                    }
                }],
               onDblClickRow: function (rowIndex, rowData) {
                	
                    $('#grid').datagrid('uncheckAll');
                    $('#grid').datagrid('checkRow', rowIndex);
                    //ShowEditOrViewDialog();
                },
                onClickRow: function (rowIndex, rowData) {
                	loadProps(rowData.id);
                    $('#grid').datagrid('uncheckAll');
                    $('#grid').datagrid('checkRow', rowIndex);
                    //ShowEditOrViewDialog();
                }
            })
        };
		function ftpConfig(nId){
        var editDialog = $('<div id="configDiv"/>').appendTo('body');
        var nodeId="-1";
        if(nId==null){
	        var node=$('#grid').datagrid("getSelected");
			if(node==null){
				msgShow("错误","未选择行","error");
				return;
			}
	        nodeId=node.id;
        }else{
        	nodeId=nId;
        }
        $(editDialog).dialog({
        	modal:true,
        	title:'FTP参数设置',
        	shadow:true,
        	iconCls:'icon-edit',
        	width:600,
        	height:500,
        	resizable:true,
        	toolbar:[{
                    text:'保存',
                    iconCls:'icon-save',
                    handler:function(){
                        if($("#editiframe")[0].contentWindow.submit()){
	                        $(editDialog).dialog("close");
	                        $(editDialog).remove();
							reload();
                        }else{
							msgShow("错误","操作失败","error");
                        }
                    }
                },'-',{
                    text:'取消',
                    iconCls:'icon-cancel',
                    handler:function(){
                         $(editDialog).dialog("close");
	                     $(editDialog).remove();
                    }
                }],
        	content:'<iframe id="editiframe" width="500px" height="455px" scrolling="no" frameborder="no" style="overflow:hidden;" src="'+basePath+'/Manage/SubSite/toFtpConfig.do?siteId='+nodeId+'&t='+new Date()+'"></iframe>'
        });
        $(editDialog).dialog("open");
		};
        function ShowEditDialog(nId){
        var editDialog = $('<div id="editNodeDiv"/>').appendTo('body');
        var nodeId="-1";
        if(nId==null){
	        var node=$('#grid').datagrid("getSelected");
			if(node==null){
				msgShow("错误","未选择行","error");
				return;
			}
	        nodeId=node.id;
        }else{
        	nodeId=nId;
        }
        $(editDialog).dialog({
        	modal:true,
        	title:'更新',
        	shadow:true,
        	iconCls:'icon-edit',
        	width:600,
        	height:500,
        	resizable:true,
        	toolbar:[{
                    text:'保存',
                    iconCls:'icon-save',
                    handler:function(){
                        if($("#editiframe")[0].contentWindow.submit()){
	                        $(editDialog).dialog("close");
	                        $(editDialog).remove();
							reload();
                        }else{
							msgShow("错误","操作失败","error");
                        }
                    }
                },'-',{
                    text:'取消',
                    iconCls:'icon-cancel',
                    handler:function(){
                         $(editDialog).dialog("close");
	                     $(editDialog).remove();
                    }
                }],
        	content:'<iframe id="editiframe" width="500px" height="455px" scrolling="no" frameborder="no" style="overflow:hidden;" src="'+basePath+'/Manage/SubSite/toEdit.do?id='+nodeId+'&t='+new Date()+'"></iframe>'
        });
        $(editDialog).dialog("open");
        };
        function ShowAddDialog(nId){
        var addDialog = $('<div id="addNodeDiv"/>').appendTo('body');
        var nodeId="-1";
        if(nId==null){
	        nodeId="<%=request.getParameter("id") %>";
        }else{
        	nodeId=nId;
        }
        $(addDialog).dialog({
        	modal:true,
        	title:'添加',
        	shadow:true,
        	iconCls:'icon-add',
        	width:600,
        	height:500,
        	resizable:true,
        	toolbar:[{
                    text:'保存',
                    iconCls:'icon-save',
                    handler:function(){
                        if($("#addiframe")[0].contentWindow.submit()){
	                        $(addDialog).dialog("close");
	                        $(addDialog).remove();
							reload();
                        }else{
							msgShow("错误","操作失败","error");
                        }
                    }
                },'-',{
                    text:'取消',
                    iconCls:'icon-cancel',
                    handler:function(){
                         $(addDialog).dialog("close");
	                     $(addDialog).remove();
                    }
                }],
        	content:'<iframe id="addiframe" width="585px" height="455px" scrolling="no" frameborder="no" style="overflow:hidden;" src="'+basePath+'/Manage/SubSite/toAdd.do?t='+new Date()+'"></iframe>'
        });
        $(addDialog).dialog("open");
        };
        //下面是用来测试属性配置部分的
        function loadProps(id){
        $("#propsPanel").attr("src","<%=path %>/Manage/SubSite/props.do?id="+id+"&t="+new Date());
        };
			
     </script>
	</body>
</html>
