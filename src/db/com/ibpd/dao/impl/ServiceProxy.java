/*    */ package com.ibpd.dao.impl;
/*    */ 
/*    */ import java.lang.reflect.InvocationHandler;
/*    */ import java.lang.reflect.Method;
/*    */ 
/*    */ public class ServiceProxy
/*    */   implements InvocationHandler
/*    */ {
/*    */   private Object delegate;
/*    */ 
/*    */   public ServiceProxy(Object obj)
/*    */   {
/* 11 */     this.delegate = obj;
/*    */   }
/*    */ 
/*    */   public Object invoke(Object proxy, Method method, Object[] args)
/*    */     throws Throwable
/*    */   {
/* 17 */     Object retObj = method.invoke(this.delegate, args);
/* 18 */     return retObj;
/*    */   }
/*    */ }