package com.ibpd.henuocms.web.controller.manage;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.dao.impl.DaoImpl;
import com.ibpd.dao.impl.IBaseService;
import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.entity.ModelEntity;
import com.ibpd.henuocms.entity.PageTemplateEntity;
import com.ibpd.henuocms.service.pageTemplate.IPageTemplateService;
import com.ibpd.henuocms.service.pageTemplate.PageTemplateServiceImpl;

@Controller
public class Template extends BaseController {
	private final String DEFAULT_TEXT="<html>\n<head>\n<title>demo</title>\n</head>\n<body>\n</body>\n</html>";
	private IPageTemplateService templateService=null;
	@RequestMapping("manage/template/index.do")
	public String index() throws IOException{
		return "manage/template/index";
	}	
	@RequestMapping("manage/template/doAdd.do")
	public String doAdd(String templateName,Integer templateType,String group,Integer order,String text) throws IOException{
		/*
		 * ID
		 * 模板名称
		 * 模板类型（站点、栏目、内容、表单）
		 * 所属分组
		 * 创建时间
		 * 更新时间
		 * 排序
		 * 模板具体内容
	*/

		templateService=(IPageTemplateService) ((templateService==null)?ServiceProxyFactory.getServiceProxy(PageTemplateServiceImpl.class):templateService);
		templateService.setDao(new DaoImpl());
		PageTemplateEntity te=new PageTemplateEntity();
//		te.setCreateDate(new Date());
//		te.setGroup(group);
//		te.setOrder(order);
//		te.setTemplateName(templateName);
//		te.setTemplateType(templateType);
//		te.setText(text);
//		te.setUpdateTime(new Date());
		templateService.saveEntity(te);
		return "redirect:/manage/template/index.do";
	}
	@RequestMapping("manage/template/getText.do")
	public String getText(Long id,org.springframework.ui.Model model){
		templateService=(IPageTemplateService) ((templateService==null)?ServiceProxyFactory.getServiceProxy(PageTemplateServiceImpl.class):templateService);
		templateService.setDao(new DaoImpl());
		PageTemplateEntity t=templateService.getEntityById(id);
		if(t!=null){
//			model.addAttribute("id",id.toString());
//			model.addAttribute("text",t.getText().trim().equals("") || t.getText()==null?DEFAULT_TEXT:t.getText());
			return "manage/template/text";
		}else{
			return ERROR_PAGE;
		}
	}
	public String saveText(Long id,String text){
		templateService=(IPageTemplateService) ((templateService==null)?ServiceProxyFactory.getServiceProxy(PageTemplateServiceImpl.class):templateService);
		templateService.setDao(new DaoImpl());
		PageTemplateEntity t=templateService.getEntityById(id);
		if(t!=null){
//			t.setText(text);
//			templateService.saveEntity(t);
			return "manage/template/index";
		}else{
			return ERROR_PAGE;
		}
		
	}
	@RequestMapping("manage/template/doEdit.do")
	public String doEdit(Long id,String group,Integer order,String templateName,Integer templateType) throws IOException{
		templateService=(IPageTemplateService) ((templateService==null)?ServiceProxyFactory.getServiceProxy(PageTemplateServiceImpl.class):templateService);
		templateService.setDao(new DaoImpl());
		PageTemplateEntity t=templateService.getEntityById(id);
		if(t!=null){
//			t.setGroup(group);
//			t.setOrder(order);
//			t.setTemplateName(templateName);
//			t.setTemplateType(templateType);
//			t.setUpdateTime(new Date());
			templateService.saveEntity(t);	
		}else{
			//还没考虑好怎么捕获异常
			return ERROR_PAGE;
		}
		return "redirect:/manage/template/index.do";
	}
	@RequestMapping("manage/template/doDel.do")
	public String doDel(String ids) throws IOException{
		templateService=(IPageTemplateService) ((templateService==null)?ServiceProxyFactory.getServiceProxy(PageTemplateServiceImpl.class):templateService);
		templateService.setDao(new DaoImpl());
		templateService.batchDel(ids);
		return "redirect:/manage/template/index.do";
	}
	@RequestMapping("manage/template/list.do")
	public void getList(HttpServletRequest req,HttpServletResponse resp,String modelId,String order,Integer page,Integer rows,String sort,String queryData){
		templateService=(IPageTemplateService) ((templateService==null)?ServiceProxyFactory.getServiceProxy(PageTemplateServiceImpl.class):templateService);
		templateService.setDao(new DaoImpl());
		super.getList(req, templateService, resp, order, page, rows, sort, queryData);
	}
	@Override
	public void getList(HttpServletRequest req,IBaseService service,HttpServletResponse resp,String order,Integer page,Integer rows,String sort,String queryData){
		order=(order==null)?"id":order;
		sort=(sort==null)?"asc":sort;
		page=(page==null)?1:page;
		rows=(rows==null)?10:rows;
		queryData=(queryData==null || queryData.equals(""))?null:queryData;
		String whereStr="";
		if(queryData!=null){
			whereStr=" where "+queryData;
		} 
		List<ModelEntity> etList=service.getList("select templateName,templateType,group,createTime,,updateTimeOrder from "+service.getTableName()+whereStr,null,order+" "+sort,  rows,rows*(page-1));
		Long rowCount=service.getRowCount(whereStr,null);
		try {
		    JSONArray jsonArray = JSONArray.fromObject( etList );
		    
			PrintWriter out = resp.getWriter();
			 out.print("{\"total\":"+rowCount.toString()+",\"rows\":"+jsonArray+"}");
			 out.flush();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
