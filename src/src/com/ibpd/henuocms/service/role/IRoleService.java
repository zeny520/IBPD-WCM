package com.ibpd.henuocms.service.role;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.henuocms.entity.RoleEntity;

public interface IRoleService extends IBaseService<RoleEntity> {
}
 