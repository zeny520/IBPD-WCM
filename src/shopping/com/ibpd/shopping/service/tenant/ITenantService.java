package com.ibpd.shopping.service.tenant;

import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.shopping.entity.TenantEntity;

public interface ITenantService extends IBaseService<TenantEntity> {
	List<TenantEntity> getListByIds(String[] ids);
}
