package com.ibpd.shopping.service.orderDetail;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.shopping.entity.OrderdetailEntity;
@Service("orderdetailService")
public class OrderdetailServiceImpl extends BaseServiceImpl<OrderdetailEntity> implements IOrderdetailService {
	public OrderdetailServiceImpl(){
		super();
		this.tableName="OrderdetailEntity";
		this.currentClass=OrderdetailEntity.class;
		this.initOK();
	}

	public List<OrderdetailEntity> getListByOrderId(Long orderId) {
		return this.getList("from "+getTableName()+" where orderID="+orderId, null, "id desc", 1000, 0);
	}

	public void deleteByOrderId(Long orderId) {
		List<OrderdetailEntity> l=getListByOrderId(orderId);
		if(l!=null){
			for(OrderdetailEntity d:l){
				this.deleteByPK(d.getId());
			}
		}
		
	}
}
