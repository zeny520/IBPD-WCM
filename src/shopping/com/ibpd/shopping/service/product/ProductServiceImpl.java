package com.ibpd.shopping.service.product;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.shopping.assist.ExtAttrEntity;
import com.ibpd.shopping.assist.ExtProductAttrEntity;
import com.ibpd.shopping.assist.ExtProductEntity;
import com.ibpd.shopping.entity.ProductAttrDefineEntity;
import com.ibpd.shopping.entity.ProductAttrValueEntity;
import com.ibpd.shopping.entity.ProductEntity;
import com.ibpd.shopping.entity.SpecEntity;
import com.ibpd.shopping.service.spec.ISpecService;
import com.ibpd.shopping.service.spec.SpecServiceImpl;
@Service("productService")
public class ProductServiceImpl extends BaseServiceImpl<ProductEntity> implements IProductService {
	private Logger log=Logger.getLogger(ProductServiceImpl.class);
	public ProductServiceImpl(){
		super();
		this.tableName="ProductEntity";
		this.currentClass=ProductEntity.class;
		this.initOK();
	}

	public List<ProductEntity> getListByIds(String[] ids) {
		if(ids!=null && ids.length>0){
			String hql="from "+getTableName()+" where ";
			for(String id:ids){
				hql+="id="+id+" or ";
			}
			hql=hql.substring(0,hql.length()-4);
			return getList(hql,null);
		}
		return null;
	}

	public List<ProductEntity> getListByCatalogId(Long catalogId,
			Integer pageSize, Integer pageIndex, String queryString,String orderType) {
		String cataId=catalogId.toString();
		String query="";
		queryString=(queryString==null)?"":queryString;
		if(!StringUtils.isBlank(queryString)){
			String[] pms=queryString.split(";");
			for(String pm:pms){
				String[] _pm=pm.split(":");
				if(_pm.length==3){
					query+=_pm[0]+" like '%"+_pm[2]+"%' and ";
				}
				
			}
		}
		String hql="from "+getTableName()+" where catalogId="+cataId+" and status="+ProductEntity.Status_Pass;
		if(!StringUtils.isBlank(query)){
			query=query.substring(0,query.length()-5);
			hql+=" and ("+query+")";
		}
		log.error("hql="+hql);
		return this.getList(hql,null,orderType,pageSize,pageIndex);
	}

	public Long getRowCount(Long catalogId, String queryString) {
		String query="";
		queryString=(queryString==null)?"":queryString;
		if(!StringUtils.isBlank(queryString)){
			String[] pms=queryString.split(";");
			for(String pm:pms){
				String[] _pm=pm.split(":");
				if(_pm.length==3){
					query+=_pm[0]+" like '"+_pm[2]+"%' or ";
				}
				
			}
		}
		String hql=" where catalogId="+catalogId+" and status="+ProductEntity.Status_Pass;
		if(!StringUtils.isBlank(query)){
			query=query.substring(0,query.length()-4);
			hql+=" and ("+query+")";
		}
		log.error("hql="+hql);
		return super.getRowCount(hql, null);
	}

	public List<ExtProductEntity> getExtListByCatalogId(Long catalogId,
			Integer pageSize, Integer pageIndex, String queryString,
			String orderType) {
		List<ProductEntity> prodList=getListByCatalogId(catalogId,pageSize,pageIndex,queryString,orderType);
		if(prodList==null || prodList.size()==0)
			return null;
		List<ExtProductEntity> extList=new ArrayList<ExtProductEntity>();
		IProductAttrValueService valueServ=(IProductAttrValueService) ServiceProxyFactory.getServiceProxy(ProductAttrValueServiceImpl.class);
		IProductAttrDefineService attrServ=(IProductAttrDefineService) ServiceProxyFactory.getServiceProxy(ProductAttrDefineServiceImpl.class);
		List<ProductAttrDefineEntity> attrList=attrServ.getParamListByCatalogId(catalogId);
		attrList=(attrList==null)?new ArrayList<ProductAttrDefineEntity>():attrList;
		ISpecService specServ=(ISpecService) ServiceProxyFactory.getServiceProxy(SpecServiceImpl.class);
		for(ProductEntity p:prodList){
			List<SpecEntity> specList=specServ.getProductSpecList(p.getId());
			List<ExtProductAttrEntity> linkList=new ArrayList<ExtProductAttrEntity>();
			for(ProductAttrDefineEntity attr:attrList){
				List<ProductAttrValueEntity> tmpList=valueServ.getAttributeLinkList(attr.getId(), p.getId());
				if(tmpList!=null){
					for(ProductAttrValueEntity v:tmpList){
						ExtProductAttrEntity extAttr=new ExtProductAttrEntity();
						extAttr.setAttrValue(v.getValue());
						extAttr.setAttrId(v.getAttrId());
						extAttr.setProductId(v.getProductId());
						ProductAttrDefineEntity d=attrServ.getEntityById(v.getAttrId());
						extAttr.setAttrName((d==null)?"":d.getAttrName());
						linkList.add(extAttr);
					}
				}
			}
			try {
				extList.add(new ExtProductEntity(p,linkList,specList));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return extList;
	}

	public ExtProductEntity getProductInfo(Long productId) {
		ProductEntity product=this.getEntityById(productId);
		if(product==null)
			return null;
		IProductAttrValueService valueServ=(IProductAttrValueService) ServiceProxyFactory.getServiceProxy(ProductAttrValueServiceImpl.class);
		IProductAttrDefineService attrServ=(IProductAttrDefineService) ServiceProxyFactory.getServiceProxy(ProductAttrDefineServiceImpl.class);
		List<ProductAttrDefineEntity> attrList=attrServ.getParamListByCatalogId(product.getCatalogId());
		attrList=(attrList==null)?new ArrayList<ProductAttrDefineEntity>():attrList;
		ISpecService specServ=(ISpecService) ServiceProxyFactory.getServiceProxy(SpecServiceImpl.class);
		List<SpecEntity> specList=specServ.getProductSpecList(product.getId());
		List<ExtProductAttrEntity> linkList=new ArrayList<ExtProductAttrEntity>();
		for(ProductAttrDefineEntity attr:attrList){
			List<ProductAttrValueEntity> tmpList=valueServ.getAttributeLinkList(attr.getId(),productId);
			if(tmpList!=null){
				for(ProductAttrValueEntity v:tmpList){
					ExtProductAttrEntity extAttr=new ExtProductAttrEntity();
					extAttr.setAttrValue(v.getValue());
					extAttr.setAttrId(v.getAttrId());
					extAttr.setProductId(v.getProductId());
					ProductAttrDefineEntity d=attrServ.getEntityById(v.getAttrId());
					extAttr.setAttrName((d==null)?"":d.getAttrName());
					linkList.add(extAttr);
				}
			}
		}
		try {
			return new ExtProductEntity(product,linkList,specList);
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public List<ProductEntity> getAboutProductList(Long productId) {
		ProductEntity pe=this.getEntityById(productId);
		if(pe==null)
			return null;
		Long cataId=pe.getCatalogId();
		String searchKey=pe.getSearchKey();
		String hql="from "+getTableName()+" where catalogId="+cataId+" and id<>"+productId;
		if(searchKey!=null && !StringUtils.isBlank(searchKey)){
			hql+=" or title like '%"+searchKey+"%'";
		}
		List<ProductEntity> l=this.getList(hql, null, "updatetime desc", 20, 0);
		if(l==null || l.size()==0){
			return null;
		}
		ProductEntity clearProd=null;
		for(ProductEntity p:l){
			if(p.getId().equals(productId)){
				clearProd=p;
				break;
			}
		}
		if(clearProd!=null){
			l.remove(clearProd);
		}
		return l;
	}

	public List<ProductEntity> getListByTenantId(Long tenantId,Integer pageSize) {
		return this.getList("from "+this.getTableName()+" where tenantId="+tenantId, null,null,pageSize,0);
	}

}
