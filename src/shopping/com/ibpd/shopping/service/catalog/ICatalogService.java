package com.ibpd.shopping.service.catalog;

import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.shopping.entity.CatalogEntity;

public interface ICatalogService extends IBaseService<CatalogEntity> {
	List<CatalogEntity> getList(Long parentId);
	List<CatalogEntity> getList(String type);
	
}
