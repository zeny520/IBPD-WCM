package com.ibpd.shopping.assist;

import java.lang.reflect.InvocationTargetException;

import com.ibpd.shopping.entity.AddressEntity;
import com.ibpd.shopping.entity.CartEntity;
import com.ibpd.shopping.entity.ExpressEntity;

public class ExtCartEntity extends CartEntity {

	private Long expressId=-1L;
	private ExpressEntity express;
	private AddressEntity address;
	public  ExtCartEntity(CartEntity cartEntity){
		try {
			InterfaceUtil.swap(cartEntity, this);
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void setExpressId(Long expressId) {
		this.expressId = expressId;
	}
	public Long getExpressId() {
		return expressId;
	}
	public void setAddress(AddressEntity address) {
		this.address = address;
	}
	public AddressEntity getAddress() {
		return address;
	}
	public void setExpress(ExpressEntity express) {
		this.express = express;
	}
	public ExpressEntity getExpress() {
		return express;
	}
}
